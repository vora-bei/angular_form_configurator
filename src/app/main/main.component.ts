import { Component, OnInit } from '@angular/core';

interface Item {
  name: string;
  open?: boolean;
  type: 'string'|'number'|'array';
  enum?: number[]|string[];
  items?:{
    type: 'string'|'number';
    enum: number[]|string[];
  }
  label?: string;
  required?: boolean;
  isNotEmpty?: boolean;// required
  minLength?: number;//
  ui_widget?:'radio'|'textarea'
  ui_description?: string;
  ui_placehoolder?: string;
  ui_options?: {
    breackAfter: boolean;
    label: string;
    [name: string]: string| boolean;
  }
}
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  types: string[] =  ['string', 'number', 'array'];
  simple_types: string[] = ['string', 'number'];
  ui_widgets: string[] = ['radio', 'textarea'];
  items: Item[] = [{
    name: "item",
    label: "ддд",
    required: true,
    type: 'string',
    }];
  remove(item: Item){
    this.items = this.items.filter(i=>i!==item);
  } 
  up(item: Item){
    const index = this.items.findIndex(i=>i===item);
    const items = [...this.items];
    items.splice(index, 1)
    items.splice(index + 1, 0, item);
    this.items =items;
    } 
  down(item: Item){
    const index = this.items.findIndex(i=>i===item);
    const items = [...this.items];
    items.splice(index, 1)
    items.splice(index - 1, 0, item); 
    this.items =items;
  } 
    
  add(){
    this.items.push({
      name: "",
      label: "",
      required: true,
      type: 'string',
    })
  }
  constructor() { }

  ngOnInit(): void {
  }

}
